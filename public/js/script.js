$(function() {
    
    $('.tombolTambahData').on('click', function() {
        $('#addMahasiswaLabel').html('Tambah Data Mahasiswa');
        $('.modal-footer button[type=submit]').html('Tambah Data');  
    });

    $('.tampilModalUbah').on('click', function() {
        $('#addMahasiswaLabel').html('Ubah Data Mahasiswa');      
        $('.modal-footer button[type=submit]').html('Ubah Data');  
        $('.modal-body form').attr('action', 'http://localhost/phpMVC/public/mahasiswa/ubah');  
           
        
        const id = $(this).data('id');

        $.ajax({
            url: 'http://localhost/phpMVC/public/mahasiswa/edit',
            data: { id: id },
            method: 'post',
            dataType: 'json',
            success: function(data) {               
                $('#nama').val(data.nama);
                $('#nik').val(data.nik);
                $('#email').val(data.email);
                $('#jurusan').val(data.jurusan);
                $('#id').val(data.id);
            }
        });

    });

});