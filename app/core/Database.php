<?php

class Database {
    private $host = DB_HOST;
    private $user = DB_USER;
    private $password = DB_PASS;
    private $db_name = DB_NAME;

    private $dbh; // databases handler
    private $stmt; // statment

    public function __construct()
    {
       //data source name
       $dsn = 'mysql:host='. $this->host .';dbname='. $this->db_name;

    //  $dsn = 'mysql:host=localhost;dbname=phpmvc';

       $option = [
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
       ];

       try {
            $this->dbh = new PDO($dsn, $this->user, $this->password);
            // $this->dbh = new PDO($dsn, 'root', '');
       } catch (PDOException $e) {
            die($e->getMessage());
       }
    }

    public function query($query)
    {
        $this->stmt = $this->dbh->prepare($query);
    }

    public function bind($param, $value, $type = null)
    {
        if (is_null($type)) {
            switch( true ) {
                case is_int($value) :
                    $type = PDO::PARAM_INT;
                    break;
                case is_bool($value) :
                    $type = PDO::PARAM_BOOL;
                    break;
                case is_null($value) :
                    $type = PDO::PARAM_NULL;
                    break;
                 default :
                    $type = PDO::PARAM_STR;
                    break;
            }
        }       

        $this->stmt->bindValue($param, $value, $type);
    }

    // execute this db

    public function execute()
    {
        $this->stmt->execute();
    }

    //output result all data

    public function resultSet()
    {
       $this->execute();
       return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function single()
    {
       $this->execute();
       return $this->stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function rowCount()
    {
        return $this->stmt->rowCount();
    }

    
}