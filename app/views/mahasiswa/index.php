<div class="container mt-4">
    <driv class="row">
        <div class="col-6">
            <?php Flasher::flash(); ?>
        </div>
    </driv>
    <div class="row mb-3">
        <div class="col-6">
            <button type="button" class="btn btn-sm btn-primary tombolTambahData" data-bs-toggle="modal" data-bs-target="#addMahasiswa">
                Tambah Data Mahasiswa
            </button> 
        </div>
    </div>
    <div class="row mb-3">
        <div class="col-6">
            <form action="<?= BASEURL; ?>/mahasiswa/cari" method="post">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="cari mahasiswa. ..." aria-label="Recipient's username" aria-describedby="tombolCari" name="keyword" id="keyword" autocomplate="off">
                    <button class="btn btn-outline-primary" type="submit" id="tombolCari">Cari</button>
                </div>
            </form>
        </div>
    </div>
   <div class="row">
        <div class='col-6'>                       
            <h1>Daftar Mahasiswa</h1>
            <ul class='list-group'>
                <?php foreach( $data['mhs'] as $mhs ) :?>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        <div class="ms-2 me-auto">
                            <div><h6><?= $mhs['nama'] ?></h6></div>
                        </div>             
                        <a href="<?= BASEURL; ?>/mahasiswa/detail/<?= $mhs['id']; ?>" class="badge bg-primary rounded-pill">Detail</a>                        
                        <a href="<?= BASEURL; ?>/mahasiswa/edit/<?= $mhs['id']; ?>" class="badge bg-warning rounded-pill mx-1 tampilModalUbah" data-bs-toggle="modal" data-bs-target="#addMahasiswa" data-id="<?= $mhs['id']?>">Edit</a>                        
                        <a href="<?= BASEURL; ?>/mahasiswa/destroy/<?= $mhs['id'];?>" class="badge bg-danger rounded-pill" onclick="return confirm('yakin');">Delete</a>
                    </li>
                <?php endforeach?>
            </ul>
        </div>
   </div>
</div>

<!-- Modal -->
<div class="modal fade" id="addMahasiswa" tabindex="-1" aria-labelledby="addMahasiswaLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addMahasiswaLabel">Tambah Data Mahasiswa</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="<?= BASEURL; ?>/mahasiswa/tambah" method="post">
            <input type="hidden" name="id" id="id">
            <div class="mb-3">
                <label for="nama" class="form-label">Nama</label>
                <input type="text" class="form-control" id="nama" name="nama">
            </div>
            <div class="mb-3">
                <label for="nik" class="form-label">NIK</label>
                <input type="number" class="form-control" id="nik" name="nik">
            </div>
            <div class="mb-3">
                <label for="email" class="form-label">Email</label>
                <input type="email" class="form-control" id="email" name="email">
            </div>
            <div class="mb-3">
                <label for="jurusan">Jurusan</label>
                <select name="jurusan" id="jurusan" class="form-control">
                    <option value="Teknik Informatika">Teknik Informatika</option>
                    <option value="Ekonomie">Ekonomi </option>
                </select>
            </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Tambah Data</button>
      </div>
        </form>
    </div>
  </div>
</div>
