<div class="container mt-4">
    <div class="card">
        <div class="card-body">
            <h5 class="card-title"> <?= $data['mhs']['nama']; ?> <a href="<?= BASEURL; ?>/mahasiswa" class='badge bg-primary rounded-pill float-end'>back</a></h5>
            <div class="card-subtitle">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item"><?= $data['mhs']['nik']; ?></li>
                    <li class="list-group-item"><?= $data['mhs']['email']; ?></li>
                    <li class="list-group-item"><?= $data['mhs']['jurusan']; ?></li>
                </ul>
            </div>            
        </div>
    </div>
</div>